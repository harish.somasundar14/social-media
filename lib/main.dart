import 'package:flutter/material.dart';
import 'package:social_media/view/pages/home_page.dart';
import 'package:social_media/view/presentation/themes.dart';

void main() => runApp(SocialMedia());

class SocialMedia extends StatelessWidget {
  const SocialMedia({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.blueAccent,
        brightness: Brightness.light,
        textTheme: TextTheme(
          headline6: TextThemes.title,
          subtitle2: TextThemes.subtitle,
          bodyText2: TextThemes.body1,
        ),
      ),
      home: HomePage(),
    );
  }
}